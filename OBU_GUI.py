import time
import tkinter as tk
import customtkinter as ctk
import tkinter.font
import threading

ctk.set_appearance_mode("dark")
ctk.set_default_color_theme("blue")


class App(ctk.CTk):
    WINDOW_WIDTH = 0
    WINDOW_HEIGHT = 0

    def __init__(self):
        super().__init__()

        print(tkinter.font.families())
        self.protocol("WM_DELETE_WINDOW", self.on_closing) 

        self.app_thread = 0
        self.speed = 0
        self.running = False
        self.snowflake_bitmap = tk.BitmapImage(file="snowflake.xbm", foreground="white", background="black")
        self.drop_bitmap = tk.BitmapImage(file="drop.xbm", foreground="white", background="black")

        # self.WINDOW_WIDTH = self.winfo_screenwidth()
        # self.WINDOW_HEIGHT = self.winfo_screenheight()
        self.WINDOW_WIDTH = 800
        self.WINDOW_HEIGHT = 425
        print(f"Screen resolution: {self.WINDOW_WIDTH}x{self.WINDOW_HEIGHT}")

        self.TEXT_RATIO = 5

        self.geometry(f"{self.WINDOW_WIDTH}x{self.WINDOW_HEIGHT}")
        self.title("OBU GUI")
        self.fg_color = "#000"
        self.configure(background = "black")

        self.frame_speed = ctk.CTkFrame(master = self, width = int(self.WINDOW_WIDTH / 2), height = int(self.WINDOW_HEIGHT / 2), corner_radius = 0, border_width=4, border_color="#fff", highlightbackground = "#fff", highlightthickness = 1)
        self.frame_speed.grid(row = 0, column = 0, sticky = "nswe")
        self.update_idletasks()
        self.speed_text = ctk.CTkLabel(master = self.frame_speed, text = "Speed", text_font = ("Roboto medium", 12), text_color = "#fff", fg_color = self.fg_color)
        self.speed_text.grid(row = 0, column = 0, sticky = "nswe", columnspan = 2)
        self.update_idletasks()
        self.speedometer = ctk.CTkLabel(master = self.frame_speed, text=f"{self.speed}", text_font=("Verdana", int(self.WINDOW_HEIGHT / self.TEXT_RATIO)), fg_color = self.fg_color, width = int(self.WINDOW_HEIGHT *2 / self.TEXT_RATIO), anchor="se")
        self.speedometer.grid(row = 1, column = 0, pady = 0, padx = 0, sticky = "nswe")
        self.update_idletasks()
        self.speedometer_text = ctk.CTkLabel(master = self.frame_speed, text = " km/h", text_font=("Verdana", int(self.WINDOW_HEIGHT / (self.TEXT_RATIO * 1.2))), anchor="sw", fg_color = self.fg_color)
        self.speedometer_text.grid(row = 1, column = 1, padx = 0, pady = 0, sticky = "nswe")
        self.update_idletasks()

        self.frame_icon = ctk.CTkCanvas(master = self, width=self.WINDOW_WIDTH - self.frame_speed.winfo_width() - 2, height=100, highlightbackground = "#fff", highlightthickness = 1, bg="black")
        self.frame_icon.grid(row = 0, column = 1, sticky = "nswe")
        self.update_idletasks()
        self.frame_icon.create_image(self.frame_icon.winfo_width() / 2, self.frame_icon.winfo_height() / 2, image=self.snowflake_bitmap)
        self.update_idletasks()

        self.frame_warning = ctk.CTkFrame(master = self, width=self.WINDOW_WIDTH, height=self.WINDOW_HEIGHT - self.frame_speed.winfo_height(), highlightbackground = "#fff", highlightthickness = 1, bg="black")
        self.frame_warning.grid(row = 1, column =0, columnspan = 2, sticky = "nswe")
        self.update_idletasks()
        self.warning_text = ctk.CTkLabel(master = self.frame_warning, text = "Icy Road", text_font=("Verdana", int(self.WINDOW_HEIGHT / (self.TEXT_RATIO * 1.2))), width=self.WINDOW_WIDTH - 2, height=self.WINDOW_HEIGHT - self.frame_speed.winfo_height() - 2, fg_color=self.fg_color, anchor="center")
        self.warning_text.grid(row = 0, column = 0, sticky = "nswe", pady = 0, padx = 0)

        # fonts
        # MS Gothic
        # Courier
        # Arial Narrow
        # Calisto MT
        # Century
        # Eras Bold ITC
        # Eras Demi ITC
        # Bitstream Charter

        self.start_thread()

    def start_thread(self):
        self.running = True
        self.app_thread = threading.Thread(target=self.thread_function)
        self.app_thread.start()

    def thread_function(self):
        max_speed = 45
        speed_dir = 1
        while self.running:
            self.speed += speed_dir
            if (self.speed >= max_speed):
                speed_dir = -1

            elif (self.speed < 0):
                self.speed = 0
                speed_dir = 1
            
            # print(self.speed)
            self.speedometer.set_text(f"{self.speed}")
            time.sleep(0.500)
        print("Thread stopped")

    def stop(self):
        self.running = False
        print("Stopping thread")

    def on_closing(self):
        self.running = False
        self.destroy()



def main():
    # window = ctk.CTk()
    app = App()

    app.mainloop()

    app.stop()


if __name__ == '__main__':
    main()